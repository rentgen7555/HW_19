﻿// HW_19.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class Animal
{
private:
    //int x;
public:
    Animal()
    {
        std::cout << "Animal\n";
    }

    virtual void Voice()
    {
        std::cout << "Voice\n";
       // return x;
    }

};

class Dog : public Animal
{
private:
    //int y;
public:

    Dog()
    {
        std::cout << "Dog\n";
    }

    void Voice() override
    {
        std::cout << "Woof\n";
       // return y;
    }
};

class Cat : public Animal
{
private:
    //int y;
public:

    Cat()
    {
        std::cout << "Cat\n";
    }

    void Voice() override
    {
        std::cout << "Meow\n";
        // return y;
    }
};

class Bird : public Animal
{
private:
    //int y;
public:

    Bird()
    {
        std::cout << "Bird\n";
    }

    void Voice() override
    {
        std::cout << "Chirik Blead\n";
        // return y;
    }
};

int main()
{
    Animal* animals[3];
    animals[0] = new Dog();
    animals[1] = new Cat();
    animals[2] = new Bird();

    for(Animal* a : animals)
        a->Voice();
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
